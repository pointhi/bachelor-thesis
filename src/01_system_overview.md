
\newpage
# System Overview

<!-- Questions:

* on which projects are we based on?
* why do we used them?
* alternatives?

--->

This work would not be possible without the foundation laid by previous projects. This chapter gives some overview of the projects on which the benchmark-suite is based on.

## C-Hayai

To build a benchmark suite a benchmark harness is used to get a consistent interface and measurements. Various benchmark harnesses for C were evaluated, as well as some C++ based ones for comparison. While there are a few interesting and well working benchmarking harnesses available in C++, they are not applicable for our use-case. This is due to the fact that a C-only benchmarking suite requires a C-only benchmark harness, to support a wide variety of tools. The C++ benchmark harness evaluated for comparison were google-benchmark [@google:benchmark], Nonious [@nonius:github; @nonius:webarchive] and Hayai [@hayai:github; @hayai:bruun]. Test applications were written in every of those C++ benchmark harnesses, and it was found out they all fail running on ```lli``` after compiling them with ```wllvm```. Because this is one of the workflows which should be supported by our harness, they, therefore, proved to be not suitable for cicro.

C-only benchmarking harnesses found include quick\_benchmark [@quick_benchmark:github], which only outputs the arithmetic mean of all iterations, and C-Hayai [@c_hayai:github], which is a rewrite of the C++ based Hayai into C. While C-Hayai for example misses the machine readable output format of Hayai, it proved to be a reasonable basis for a custom benchmarking harness. C-Hayai, therefore, was improved until all requirements were satisfied. This included the addition of JSON export, improved timer handling as well as the integration of hardware counter support.

## PAPI

Modern CPU's are equipped with so-called hardware counters, which have a minimal execution overhead on usage.[@weaver2008can] They can be used to gain insights on running applications without affecting the instructions executed. This allows measurements with a minimum impact on the tested application. Metrics which can be measured using hardware counters includes clock-cycles executed, instructions executed in total as well as broken down into specific types, as well as the number of cache misses. The actual type of counters available is dependent on the CPU used.

Typically, the amount of active hardware counters usable in parallel is limited. Some specific metrics furthermore require multiple counters to be evaluated as derived metrics. To standardize the usage of hardware counters into a stable, system independent interface, PAPI [@terpstra2010collecting] was invented. It allows to interact with the hardware counter interfaces on a high abstraction level.

Using hardware counters, users can gain deep insights into the application behavior, which would be not possible with a simple time measurement. While the actual execution time is important, many properties of the hardware can influence it. Hardware counters allow to quantify the impact of a benchmark on the hardware and identify bottlenecks.

## Python 3.x

While the benchmarks itself are C-only, they need to be controlled and evaluated in an automatic way. The benchmark sources need to be compiled with the specified options by the defined compiler, and then executed each. Results from those executions need to be evaluated and exported in some way for further processing. To do all of this, ```Python 3``` was selected, which is a scripting language often used on Linux systems. It is therefore available on many systems by default.

Python [@python:website] is a dynamically typed general-purpose programming language. It allows multiple programming paradigms like procedural, object-oriented as well as functional programming. Memory is garbage-collected which simplifies programming and prevents memory errors.

It has to be noted the ```Python 2.x``` version is in some parts incompatible to the current ```Python 3``` language specification. This led to a number of projects which were not ported to the newer language specification. To be future proof, the benchmark harness was written in ```Python 3``` to be as compatible as possible on modern systems.

## Docker

Building and running applications across OS versions on a host system can be quite complex. A simple system upgrade sometimes is enough to have an application no longer building. This happens especially on projects which are no longer maintained or are based on outdated libraries. In some other cases build instructions are too complicated to easily get the application running, or there are conflicting dependencies, due to the requirement to run multiple version of the same application.

One solution for this problem is operating-system-level virtualization. Docker is such a system to run userspace components isolated from each other on a single host system.[@merkel2014docker] Due to the separation of userspace components it is possible to run different applications with otherwise mutually exclusive requirements on the same host system. In fact, it allows us to quickly get applications running on systems which are otherwise incompatible. Furthermore, userspace libraries can be tied to a specific version / operating-system-version. Docker is therefore a solution to get reproducible builds due to the usage of Dockerfiles and reproducible execution due to operating-system-level virtualization.[@chamberlain2014using; @cito2016using, @cito2017empirical]

While abstracting the user-space is a promising idea, it needs to be kept in mind that benchmarking requires consistent and reproducible system behavior. According to previous research, Docker does not impact CPU and memory usage, but I/O and OS interaction is affected.[@preeth2015evaluation; @felter2014ferreira] <!-- This leaded to the assumption that benchmarks should focus on raw computation while minimizing disk and kernel-accesses. -->
