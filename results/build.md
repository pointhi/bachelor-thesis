
Build required docker container

```
sudo make build_BENCHMARK_DIEHARD build_BENCHMARK_VALGRIND build_BENCHMARK-CLANG-3.8 build_BENCHMARK-CLANG-3.9 build_BENCHMARK-CLANG-4
```

```
make -C ./docker/BENCHMARK_CLANG-8 run OUTPUT_DIR=/home/thomas/JKU/bachelor-thesis/results BENCHMARK_ARGS='--filter-runtime="clang-O3"'
make -C ./docker/BENCHMARK_VALGRIND run OUTPUT_DIR=/home/thomas/JKU/bachelor-thesis/results BENCHMARK_ARGS='--filter-runtime="valgrind-O3"' # TODO: missing
make -C ./docker/BENCHMARK_DIEHARD run OUTPUT_DIR=/home/thomas/JKU/bachelor-thesis/results BENCHMARK_ARGS='--filter-runtime="diehard-O3"'
make -C ./docker/BENCHMARK_BOEHMGC run OUTPUT_DIR=/home/thomas/JKU/bachelor-thesis/results BENCHMARK_ARGS='--filter-runtime="boehmgc-O3"'
# TODO: address sanitizer
```
