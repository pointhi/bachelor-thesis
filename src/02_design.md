
\newpage
# Design

<!-- Questions:

* how does the suite looks like in general?
* what was the idea behind the design?
* where do I find x,y,z?
* how does it work in internal?

--->

The software architecture which was developed for cicro consists of multiple scripts which can work on the same data representation. For the implementation, C, Python as well as R is used, depending on the requirements of the specific script.

The main script ```bench.py``` handles the whole benchmarking process. It reads the configuration of a benchmark run and executes it. To do this all benchmarks are compiled with the runtime environment of interest and then executed consecutively. Intermediate results are stored in an in-memory-database and extended with information about the executing system. Benchmark data is then stored on disk in form of JSON formatted files. Those are based on the the machine-readable format Hayai is using. In cicro, this data-format was extended to contain all information a benchmark run in cicro consists of.

Based on the exported JSON file, various post-processing operations are available. The most important one is ```plot.R```, to get a visual representation of the benchmark results. It tries to output them in various types of representations which can then be used for simple visual data assessment. Beside of plotting, helper scripts are available to merge/split benchmark files or to get a quick overview what they contain. There are also scripts which help setting up a correctly working benchmarking environment. This design is visualized including their dependencies in **Figure \ref{overview}**.

![Basic overview of main parts cicro consists of and their dependencies.\label{overview}](assets/overview.pdf)

<!--cicro is a c benchmarking suite which tries to simplify the life of users as much as possible. This includes an easy install and execution process, requiring only a low amount of manual steps. Furthermore, customization of the benchmarking process should be easily doable to fit its purpose to help compiler, runtime and tool developers.

The tool, therefore, is designed to do as much as possible out-of-the-box and adapts to missing dependencies in a flexible fashion. It warns the user when conditions are detected which could interfere with the validity of the results. To get an example benchmark-run up-and-running only a single command is required. This benchmark script handles all steps from compilation up to running and storing the results.

The tools for scripting purpose are written in ```Python 3```, and scripts for visualization purpose are written in ```R```. The benchmark, as well as the basic benchmark harness, are written in pure C, to allow them to run on as many systems as possible. Benchmarks with requirements not supported by the evaluated runtime are ignored in case of necessity. This allows using the benchmark-suite on runtime environments which do not support the full C-language.

The Benchmarks itself which are executed are in fact stand-alone-applications where the benchmark-harness is linked statically. This allows for simple manual testing. Adding new benchmarks is possible by adding the required source files and extending the Makefile which handles compilation. The benchmark itself is found due to a simple naming convention (suffix of "\_test"), and included in future benchmark runs automatically.
-->

## Benchmark Selection Process

To have a representative set of benchmarks, we wanted to reduce personal bias towards the benchmark selection process. As general-purpose benchmarking suite, it should contain projects targeted towards a wide variety of problems. They should represent real-world code and therefore consist of various coding-styles/programming principles. Some of the benchmarks might be optimized on assembly level, while others do not mind performance issues. By using GitHub as data-source and a project selection based on a set of constraints, an extensive benchmark suite can be built.

As initial step a database of GitHub projects was constructed. In this case all projects tagged with the programming language C and 100-300 GitHub stars were included. Subsequent analysis was then performed on this database. The first filtering done on the projects was targeted towards the licenses used. Reason for this decision was to circumvent possible licensing issues by limiting the included benchmarks to be based on a permissive license. Licenses which were suitable to be used in the benchmark suite were MIT, Apache 2.0, BSD-2-Clause, BSD-3-Clause, zlib, CC0, and Unlicense.

<!--To reduce the bias of the benchmarks toward specific coding-styles/principles, as well as the topic of the benchmark behavior, a specific selection process was introduced. While personal influence cannot be mitigated this way, it at least reduces it to some extent.

One way to minimize the influence is to use code written by different people, and with various problems to solve. Thus, some implement data-structures, other one's scripting languages, ...
An easy way to get a huge amount of different projects is the use of a famous web-service often used by programmers, called GitHub. Using additional constraints, like the number of stars a project needs to have at the date of analysis, we get a limited set of projects to evaluate.

For evaluation, quite a bit of requirement had to be evaluated. Some of them are to decide objectively, like the license of the project. Licenses allowed to be included in the benchmark suite were limited to MIT, Apache 2.0, BSD-2-Clause, BSD-3-Clause, zlib, CC0, and Unlicense. Reason for this decision was to circumvent possible licensing issues by limiting to permissive licenses. Another objective metric which was mentioned before is the number-of-GitHub-stars.-->

Beside of those non-technical metrics, it has to be ensured that the project is able to run on the benchmark harness with minimum effort. For a C benchmark suite we had to ensure neither C++ nor Objective-C code is used. Limiting the query in GitHub to C projects is not enough to ensure this. Another relevant metric which is important to consider were external dependencies. It was decided to keep the number of required libraries low, namely only links to libc and POSIX are allowed. In the final benchmark suite, also OpenMP is included as dependency for a few benchmarks due to oversights in the selection process. Reason for this was the handling of dependencies inside the project-specific Makefile, and the existence of the library on the developer system. In the case where the used runtime environment does not support OpenMP, it can be skipped like any other failing benchmark. Benchmarks can for example also fail when the runtime environment does not support pthreads or some of the used C-language features.

When fulfilling the objective criteria, implementation of a benchmark can be started. For practical reasons, this should be possible in a timely manner. Documentation therefore serves as initial starting point if existing, otherwise test-cases or benchmarks can be used. Unfortunate, this is not always possible and manual code analysis has to be done. For benchmark implementation, the amount of changes to the up-stream project is minimized. This means projects where the main function and important program parts are located in the same file are considered unsuitable. In the case when projects have console-output, this is either disabled inside the benchmark using some workarounds, or otherwise by patching out the print statements directly. GUI applications or applications requiring interaction are for obvious reasons not usable for a benchmark suite. While network access is something not desired for this benchmark suite, disk access itself does not mark a benchmark unsuitable for inclusion. Nevertheless, read/write access to the disk is something which can lead to exclusion of the benchmark when it influences the benchmark process in a relevant way.

<!--For practical reasons, included benchmarks should be based on code easily getting a grasp of. The most practical way would be to have documentation to use, otherwise, examples/test cases/benchmarks are used if present. In case of having nothing of the resources provided stated before, the code has to be analyzed manually and a choice is made on the assumption of time required to get a benchmark built on top of the specific project. It is desired to reduce the number of modifications to the provided project files to zero if possible. Projects where main components are located in the same file as the main function, therefore, are not really suitable for inclusion. It has to be noted that some of the projects have bugfixes added, which are not always present in the up-stream-project.

Another program specific part to consider is, that the executed code should not emit any console-output. If this is the case, workarounds are used to silence printf statements, or the project needs to be edited manually. GUI applications are for obvious reasons not usable for a benchmark suite. Execution should happen automatically and should be possible on console-only systems as well. For actual benchmarking it is beneficial as well to have no GUI-applications running.

While network access is something not desired for this benchmark suite, disk access itself does not mark a benchmark unsuitable for inclusion. Nevertheless, read/write access to the disk is something which can lead to exclusion of the benchmark suite when it influences the benchmark process in a relevant way. The written benchmark itself should be quite controlled in runtime as well. It was decided a single benchmark run should happen in around 500ms on a modern system, which is quite fast. By adding additional iterations confidence in the results can be achieved. Those short benchmark runtimes allow quick benchmarking, as well as benchmarking of slow runtime environments in a reasonable timeframe. In case accuracy needs to be increased, the number of iterations can be raised.-->
<!-- TODO: talk about benchmark execution time? -->

## Project Structure

<!--
* what was the idea behind the design?
* where do I find x,y,z?
-->

The cicro benchmark suite includes a variety of components placed in their distinct folders. With the exception of few external dependencies, all required sources are located in the benchmark suite. This is especially important for the benchmark sources. Despite they are built on top of GitHub sources, it cannot be guaranteed to have them available in the future. <!--In fact, one of the sources used no longer exists. TODO: describe in a different section? -->

<!--To give a basic overview of the cicro-benchmarks project, we can look into the overall directory structure. With the exception of very few external dependencies, all required sources are located in the benchmark suite. This is important to get started quickly and to be not dependent on external resources. This is especially important for the benchmarks itself. Despite they are built on top of GitHub sources, it cannot be guaranteed to have them available in the future. In fact, one of the sources used no longer exists in the original repository.-->

### Main Benchmark Suite

For a benchmarks suite, multiple components are required. In addition to the benchmarks itself, a benchmark harness is needed to unify the measurement and their external behavior. While this is enough for simple benchmarking purpose, some additional tools are required to do benchmarking in an automated way.

The most important directories in cicro are outlined in **Table \ref{benchmark_dirs}** including some description. The directories ```./benchmarks/``` and ```./C-Hayai/``` are necessary to actually have benchmarks which can be executed. In fact, it is possible to do benchmarking using only them.

To automate the benchmark process, the script ```./tools/bench.py``` is used. It uses basic configuration specified in ```./configs/```directory to know how the runtime environment has to interact with the benchmarks. Using those snippets, it is possible to benchmark specified runtime environments with a single command line instruction. To do this, ```./tools/bench.py``` has to handle all execution steps including compilation, execution and data handling. Error handling is another part of concern, to not lose benchmark results due to faulty behavior of a runtime environment.

: Most important directories present in the cicro benchmark suite.\label{benchmark_dirs}

+------------------------+-----------------------------------------------------------------+
| Directory              | Description                                                     |
+========================+=================================================================+
| ```./benchmarks/```    | benchmarks which are executed                                   |
+------------------------+-----------------------------------------------------------------+
| ```./C-Hayai/```       | source of the benchmark harness on which benchmarks are based on|
+------------------------+-----------------------------------------------------------------+
| ```./configs/```       | configuration of runtime enviorments and envioment variables    |
+------------------------+-----------------------------------------------------------------+
| ```./tools/```         | scripts used to work with the benchmarks and run them           |
+------------------------+-----------------------------------------------------------------+

<!--The implementation of those requirements resulted in the usage of C-Hayai as benchmark harness located in ```./C-Hayai/```.
As basis for all benchmarks, ```./C-Hayai/``` is the benchmark harness-->

\newpage
### Utility Tools

In the ```./tools/``` directory there are various scripts located. As described before, ```./tools/bench.py``` serves the central role of controlling the benchmarking process. Looking on **Figure \ref{overview}** indicates that there are more scripts present to handle various tasks.

The most important script would be ```./tools/plot.R``` which actually visualizes the benchmark results. Due to the high number of data points, multiple different visualizations are created with the purpose to allow users to get a quick overview over the results. For example, execution time is plotted using a box-plot, to allow easy comparison of multiple runtime environments. Users can quickly see the calulated mean, the maximum and minimum execution time as well as the general variation of the results.

Besides plotting, supporting the benchmarking process as well as handling of the benchmark data are also important tasks. To improve benchmark results, ```./tools/ondemand.sh``` handles the CPU performance mode to be set to the appropriate value. To handle the benchmark data the script ```./tools/merge_results.py``` exists to allow combining multiple files into one, or to filter by runtime environment. This allows to do multiple small runs and plot all of them combined later on.

### Docker Executor

To simplify the benchmarking process for a wide variety of runtime environments, Docker is used. As described in the introduction, the usage of operating-system-level virtualization should improve reproducibility and allow multiple versions of the same runtime environment to exist on the same system. This requires the addition of Dockerfiles describing how those runtime environments need to be installed and executed. All directories related to Docker support are outlined in **Table \ref{docker_dirs}**, but users only require the directory ```./docker/``` when using the docker executor. This directory contains the Dockerfiles of all supported runtime environments as well as their dependencies. This allows to locally build and execute all runtime environment benchmarks created for cicro in a unified way.

<!-- To automatically trigger builds when the base image changes, there is a ```post_push``` hook added for docker cloud in the directory ```./hooks/```. <!-- TODO: activate! rewrite name docker cloud? - - > The ```./paper/``` directory mainly uses the docker executor, and is present to allow 3rd-parties to reproduce the results of a paper not published yet.-->

: Directories added for the Docker executor.\label{docker_dirs}

+------------------------+-------------------------------------------------------------------+
| Directory              | Description                                                       |
+========================+=================================================================+
| ```./docker/```        | build scripts for working docker images                           |
+------------------------+-------------------------------------------------------------------+
| ```./docker-failed/``` | build scripts for non-working docker images                       |
+------------------------+-------------------------------------------------------------------+
| ```./hooks/```         | docker hooks used to control dependencies onto docker base images |
+------------------------+-------------------------------------------------------------------+
| ```./paper/```         | build scripts to reproduce results of a scientific paper          |
+------------------------+-------------------------------------------------------------------+

<!-- TODO: docs? -->
<!--To get the benchmark suite running only the directory ```./configs/``` and ```./tools/``` are of interest for the user. In case the docker runner is used,  ```./docker/``` contains all sources required to build the images and run them. Benchmarks are located in the ```./benchmarks/``` folder and the benchmark harness can be found in ```./C-Hayai/```. There are some <y

: Directories present in the cicro benchmark suite

+------------------------+-----------------------------------------------------------------+
| Directory              | Description                                                     |
+========================+=================================================================+
| ```./benchmarks/```    | benchmarks which are executed                                   |
+------------------------+-----------------------------------------------------------------+
| ```./C-Hayai/```       | source of the bechmark harness on which benchmarks are based on |
+------------------------+-----------------------------------------------------------------+
| ```./configs/```       | configuration of runtime enviorments and envioment variables    |
+------------------------+-----------------------------------------------------------------+
| ```./docker/```        | build scripts for working docker images                         |
+------------------------+-----------------------------------------------------------------+
| ```./docker-failed/``` | build scripts for non-working docker images                     |
+------------------------+-----------------------------------------------------------------+
| ```./docs/```          | documentation and notes                                         |
+------------------------+-----------------------------------------------------------------+
| ```./hooks/```         | docker hooks used to time building                              |
+------------------------+-----------------------------------------------------------------+
| ```./paper/```         | build scripts to reproduce results of a scientific paper        |
+------------------------+-----------------------------------------------------------------+
| ```./tools/```         | scripts used to work with the benchmarks and run them           |
+------------------------+-----------------------------------------------------------------+

The most important script is ```./tools/bench.py``` which actually executes the benchmarks and handles the measurements of performance data. ```./tools/ondemand.sh``` is a helper-script to improve the accuracy of the benchmarks. This is done by disabling CPU-scaling, which otherwise increases the derivation of the measured metrics.

To get the benchmark suite running only the directory ```./configs/``` and ```./tools/``` are of interest for the user. There are various tools available to run or evaluate benchmarks. The most important script is ```./tools/bench.py``` which actually executes the benchmarks and handles the measurements of performance data. ```./ondemand.sh``` is an important helper-script to improve the accuracy of the benchmarks. This is done by disabling CPU-scaling, which otherwise increases the derivation of the measured metrics.

: Available tools in the ```./tools/``` directory

+-----------------------------------+------------------------------------------------+
| Tool                              | Description                                    |
+===================================+================================================+
| ```bench.py```                    | Main Tool to run benchmarks and store them     |
+-----------------------------------+------------------------------------------------+
| ```detect_normal_iterations.py``` |                                                |
+-----------------------------------+------------------------------------------------+
| ```extract_gcov.py```             |                                                |
+-----------------------------------+------------------------------------------------+
| ```merge_results.py```            | Merge multiple benchmark-result files together |
+-----------------------------------+------------------------------------------------+
| ```ondemand.sh```                 | Change CPU Performance mode                    |
+-----------------------------------+------------------------------------------------+
| ```plot.R```                      |                                                |
+-----------------------------------+------------------------------------------------+
| ```plot_compiler_versions.R```    |                                                |
+-----------------------------------+------------------------------------------------+
| ```plot_instruction_mix.R```      |                                                |
+-----------------------------------+------------------------------------------------+
| ```plot_loc.R```                  |                                                |
+-----------------------------------+------------------------------------------------+
| ```stats.py```                    | Print some stats (incl. missing benchmarks)    |
+-----------------------------------+------------------------------------------------+

It has to be noted that not all resources in the project are of relevance at all for users. For example the directory ```./paper``` as well as some other scripts have the only purpose to allow reproducible creation of specific measurements. They can be of interest to get an overview of execution behavior of a big variety of tools but requires quite some amount of dedication to get them fully executed.-->

\newpage
## Benchmark Harness

A benchmark harness is the framework on which the actual benchmarks are built on. The central purpose of a benchmark harness is to control the execution of the benchmark itself as well as performing all desired measurements. While doing so, the impact of the benchmark harness onto the measurements needs to be negligible to get valuable results. On top of this basic functionality, it is often desired to have the ability to modify benchmark execution due to diverse user requirements.

In the case of cicro, the benchmark harness is split into two parts, which can work independently and are based on a common interface. The first part is C-Hayai, it is the benchmark harness which controls the execution inside a benchmark file, handles measurements and their output for further processing. The second part are various scripts used to manage the benchmark process and to process the results. Those scripts can be found in the ```./tools/``` directory.

### C-Hayai

While the original C-Hayai [@c_hayai:github] provides all basic functionality of a simple C-benchmarking suite, it lacked required features like a machine-readable output format. C-Hayai was therefore extended to suite all requirements and fix problems identified. Those changes ranged from the mentioned additional of an JSON output format to improved timer handling and hardware counter support. To keep compatibility, it uses the same JSON format Hayai [@hayai:github; @hayai:bruun] exposes. This allows us to get the benchmark-data in a machine-readable format which includes all data available to the benchmark harness. Furthermore, JSON itself has the advantage to be extendable with minimum effort, which gives us a future proof implementation.

Due to the use case of cicro to benchmark JIT-compilers for C, namely Sulong [@rigger2016sulong], we added the possibility to specify warmup iterations for a benchmark. This is not a typical feature found in benchmark harnesses for compiled languages. Nevertheless, even in case of compiled code it it makes sense to support warmup iterations. By doing so, those warmup iterations can initialize the CPU cache, branch prediction, memory pages, and other performance-affecting parts of the hardware. Another improvement was the addition of hardware-counter support. This allows users to gain insights into benchmark execution in more details than a measurements of only the execution time would give.

To allow the user to control the benchmark execution, a command line parser was added. It allows the modification of warmup and benchmark iterations, output-format as well as which hardware-counters to measure. To give a better estimation of the validity of a benchmark run, the type of time source used, as well as their theoretical and practical resolution are recorded. This should increase the confidence into the results obtained, due to the fact that different systems support different time sources. While the benchmark harness tries to use the source with most-precision available, it is important to document the choice made.

### ./tools/bench.py

The script ```./tools/bench.py``` is the main script used by the user. It manages everything which is not required to be done inside the benchmark executable directly. Tasks which need to be handled are: validating the system configuration and extracting basic metrics, building and executing the benchmarks, combining results as well as error-handling for all of the steps described.

A variety of program arguments can be used to adjust the script behavior. The available runtime environments are specified in the ```./configs/``` directory and are loaded by ```./tools/bench.py``` on startup. User can then select which of the provided runtimes they want to execute, as well as control parts of the execution process. For example, skip the compilation step to speed up repeated benchmark runs, or limit the amount of benchmark executed.

### ./configs/ Directory

Parts of the benchmark process can be adjusted using python scripts. Those snippets are located in the directory ```./configs/```. They are used to register runtime environments to the benchmark script, as well as to adjust the benchmarking process to work with the desired tool. Additionally, this directory contains the file ```./configs/env``` which is used to define environment variables used by the runtime environments. This allows to define a runtime environment once, and specify the actual location of the tool at a later step.

It only requires a few lines of code to add a simple runtime environment. An example is shown in **Listing \ref{lst:scripts_gcc}** which demonstrates the addition of GCC into cicro. Nevertheless, if necessary it is possible to modify the build as well as the run phase to get more complex runtime environments working.

```{#lst:scripts_gcc .python caption="Example of how to define GCC with the flag -O0 and -O3 as a runtime environment."}
from functools import partial

# specify variables to override. In this case specifying how to
# extract version informations from "gcc" and "as".
kwargs = {'build_system_func': partial(build_system_executor,
    cc_version='--version', as_version='--version')}

# our runtime environments to register with all required flags.
harness.add_runtime('gcc-O0', {"CC": "${GCC}", "AS": "as",
                               "CFLAGS": "-std=gnu99 -O0"}, **kwargs)
harness.add_runtime('gcc-O3', {"CC": "${GCC}", "AS": "as",
                               "CFLAGS": "-std=gnu99 -O3"}, **kwargs)
```

<!--By using a separated environment file, it is simply to modify the used tool in a runtime environment. Typically they point to the default tool if possible, and can be modified when benchmarking a specific tool is desired.

```{#lst:scripts_gcc .py caption="corresponding entry in ```./configs/env``` to declare GCC environment variable"}
GCC=gcc
```-->

\newpage
### Datamodel

To store all data from the benchmarking process, and represents relations between the entries, a datamodel was defined. To give an overall overview, the schema is visualized in **Figure \ref{db_schema}**. To simplify the implementation, scripts in cicro using an in-memory database based upon this datamodel. To handle access to the database, an object-relational mapper called ```sqlalchemy``` is used. The entities of the schema are described in more detail now:

The benchmark file which is mapped to the database is represented in the table ```Harness```. Because a benchmark file can contain multiple benchmarks, those are declared inside the table ```Benchmark```. Every benchmark file execution is stored in the ```Execution``` table as well as in the ```Run``` and ```Datapoint``` table for the actual benchmark results. Because the benchmark suite handles compilation as well as configuration, those information are stored for completeness. While this information is not necessary for benchmark evaluation, they are convenient to document the benchmarked system and to allow a rough assessment of the validity of the results.

![Entity-relationship diagram of the internal database.\label{db_schema}](assets/db_schema.pdf){ width=95% }

\newpage
### File Format

The exported benchmark data is a JSON based file format derived from Hayai [@hayai:github; @hayai:bruun]. It was extended to contain the additional data-items the cicro benchmark suite records including some harness data. In the original file format, only benchmark data was recorded, but no information about the system itself. To support those additional usecases, a redesign was necessary to include harness data and benchmark data inside a single JSON-file. Linking of those two datasets is done using the ```<runtime-name>```, where the index in each list links the distinct runs of a benchmark-harness together. This file format is loosely described in **Listing \ref{lst:json_file_example}**, while omitting many details how the actual data looks like.

In the end, this resulted in a file format which is neither directly compatible to Hayai, nor a file format which can be described as simple. Part of the problem arises due to the fact of mapping a complex data-model to plain attribute-value pairs. Interconnections present in the datamodel, as visualized in **Figure \ref{db_schema}**, are hard to represent as a JSON equivalent.

```{#lst:json_file_example .json caption="Rough overview how harness and benchmark data is connected in the JSON format."}
{
  "benchmark_data": {
    "<benchmark-name>": { /* replaced by actual benchmark name */
      "<runtime-name>": [ /* replaced by actual runtime name */
        {
          "harness": "<harness-name>",  /* replaced by harness name */
          "runs": [{"duration": 123.0}, /* ,... more data */ ]
          /* additional benchmark information is located here */
        },
        /* {... additional benchmark runs have the same structure */
      ]
    }
  }
  "harness_data": {
    "<harness-name>": { /* replaced by actual harness name */
      "<runtime-name>": [ /* replaced by actual runtime name */
        { /* harness information located here */ },
        /* {... multiple benchmark runs -> more harness information */
      ]
    }
  }
}
```

\newpage
## Docker Executor

Reproducibility is an important concern in research. Results which are explainable and which can be reproduced by a 3rd party are way more trustworthy than results which lack the explanation how to obtain them. There is a vast amount of properties to consider when reproducing work, and one of the most fundamental ones are the tool dependencies and a way to install the tool. The operating system is another part which can play an important role in reproducing work.

To simplify reproduction of results, a defined software stack which has the required tools and libraries included can be quite helpful. The solution implemented in this bachelor thesis is the usage of docker. This allows to store and retrieve the userspace used for a benchmark run and enables it to get it running on another system. This includes all libraries and tools required to build the specific application. The disk volume representing the reproducible benchmark run can then be used by a 3rd party in a quick manner. This furthermore solves the hassle of obtaining and building the specific version of the tested runtime environment.
