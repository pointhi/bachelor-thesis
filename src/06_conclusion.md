
\newpage
# Conclusion

<!-- Questions:

* what is the final state, assessment?
* not usable for GC measurements

--->

With the creation of cicro, a free benchmarking library has been implemented with the target to help in research an engineering. By being free, everyone can use it to reproduce work without being forced to acquire costly license fees.

Another contribution of this bachelor thesis is the extension of C-Hayai to be a more feature complete C-benchmarking harness. It can be employed as standalone library without the dependency to any other parts of cicro. In the case a C++ benchmarking harnesses is not an option, C-Hayai should be a suitable alternative.

The evaluation gave a diverse image of the capabilities cicro delivers. The comparison to published benchmark results showed quite some differences, which can be explained to some extend. The most notable discovery could be, that cicro in its default configuration is not suitable to measure garbage collector overheads, due to the low execution time. On the other hand, it allows quick performance evaluation cycles. Runtime environments which do not show execution time dependent behavior are not affected by this problem. Dynamic runtime environments on the other hand need to have carefully assessment of the results to check their validity.

The measurements concerning repeatability resulted in a measured mean where consecutive measurements varied less than $\pm0.1\%$. This makes cicro suitable to measure performance differences below the 1% mark under consideration of statistic derivations. When increasing the number of samples and decreasing the influence of the system, even higher accuracy can be achieved.

The evaluation of the docker executor delivered the expected results of having no measurable overhead on CPU and memory usage [@preeth2015evaluation; @felter2014ferreira]. When comparing docker execution to execution directly on the host system, the measured mean had no difference which would be considered statistical relevant. This increases confidence for the approach of using docker to improve reproducibility.

While the benchmarking part of cicro looks complete for the initial version, the evaluation tools present can be considered lacking. To simplify and standardize the evaluation, more sophisticated plotting scripts need to be implemented. The R scripts currently present in cicro show multiple plots, but the most important one is not really readable in its default configuration. Therefore, manual rework of the output scripts was necessary to adjust them to the results. Otherwise, plots would loose quite some expressiveness and readers would not be able to extract information visually.
