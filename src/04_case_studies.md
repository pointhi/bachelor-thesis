
\newpage
# Case Studies

<!-- Questions:

* how does it compare to other benchmark results?
* which projects does it consist of?
* how is the overhead of the solution, accuracy, ..?

--->

We evaluated various characteristics of the benchmark suite. First, we measured the performance of various tools and compared them with the performance stated in the original paper. This allows some rough estimation how our benchmark suite compares to the measurements done in a different way. Secondly, the minimum requirement a benchmark suite has to pass is repeatability of execution. Otherwise, it cannot be taken on that the results represent performance differences accurately. Furthermore, due to the addition of the docker executor it needs to be checked that operating-system-level virtualization does not impose an overhead which distort results.

Besides of measuring the systems behavior, some summaries about the benchmarks implemented can be outlined. Due to the nature of using code found on GitHub, it was not clear at the beginning which type of application will be integrated. Due to the fact that some of the applications contained bugs, fixes were submitted to the original projects. This allows some very rough estimations to which extent known errors are corrected upstream. <!-- TODO: modify depending on what is actually described -->

<!--## Benchmark Results

The central work is building a benchmark framework. Therefore an evaluation of the finished benchmark-suite has to take place, which does not only cover benchmark results from the different runtime environment. To demonstrate that the benchmark-suite is, in fact, usable for its designed functionality, we need to measure additional parameters like repeatability and comparability with other benchmarking measurements. In our case, there is an additional requirement to validate the possible overhead of containerization and how it affects the measurement in general.-->

## Comparability

Reproducing results stated in their original papers, can be viewed as one of the ways to validate a benchmark suite. Getting similar results using a different benchmarking suite is a strong indicator that performance measurements are reproducible. Differences do not invalidate results directly, but point to interesting aspects for further investigation.

As a rough estimation, **Table \ref{case_studies_comparison}** shows results found in different papers compared to the results obtained using cicro. It has to be noted to see those results with caution. It can be assumed none of those benchmarks done used the same software version with which the benchmarks were carried out in the original papers. To some extend, it was not even clear what software was used for the original benchmark results. For this comparison, benchmarking was done using the docker executors distributed with cicro.

<!-- TODO: different OS version, compiler version,... -->

: Small comparison of benchmark results found in papers reproduced in cicro.\label{case_studies_comparison}

+---------------------------------+------------------+-------------------+-----------------+-------------------+
| Reference                       | Measured Runtime | Reference Runtime | Overhead Stated | Overhead Measured |
+=================================+==================+===================+=================+===================+
| AddressSanitizer [@2012asan]    | asan-O2-v3.9     | clang-O2-v3.9     | 73%             | 94% (1.94x)       |
+---------------------------------+------------------+-------------------+-----------------+-------------------+
| Valgrind [@2007valgrind]        | valgrind-O3      | clang-O3-v8       | 2120% (22.2x)   | 2580% (26.8x)     |
+---------------------------------+------------------+-------------------+-----------------+-------------------+
| DieHarder [@novark2010dieharder]| diehard-O3       | clang-O3-v8       | 2%              | 4% (1.04x)        |
+---------------------------------+------------------+-------------------+-----------------+-------------------+
| DieHard [@2006diehard]          | bohemgc-O3       | clang-O3-v8       | 13.4%           | 0% (1.00x)        |
+---------------------------------+------------------+-------------------+-----------------+-------------------+

It has to be noted first, that some of the benchmarks in the suite did not complete execution. In case of DieHard, one benchmark failed due to a timeout. For Bohem GC 7 out of 71 benchmarks failed due to various reasons, including missing output on stdout and triggered asserts. It can be assumed that cicro triggered some bugs in this runtime environment. Valgrind had the highest number of errors, failing 16 out of 71 benchmarks due to timeouts. The default timeout specified in cicro is 240s per benchmark file. A high runtime overhead means that the default settings can cause benchmarks to not complete as happened in the valgrind measurements.

As stated before, those results can differ quite bit from the original numbers, since the software versions used in this comparison are far more up-to-date. For comparison, the overhead stated in 2007 for DieHard [@2006diehard] was 12%, while the improved DieHarder [@novark2010dieharder] stated 2% in 2010. Our measurement resulted in an overhead estimation of 4%, with a benchmarking suite consisting of very different workloads.

AddressSanitizer also comes with results indicating a higher performance overhead as stated in the original paper. In this benchmark, a compiler version was selected which better matches the version used in the original paper. The minimum overhead measured was 0%, while the highest runtime overhead measured ranged up to 1497% excluding the one testcase with a timeout. The first quartile marked 59% overhead in the boxplot as seen in **Figure \ref{benchmark_asan}** and the third quartile marked 196% overhead. The original stated 73% are located inside the notch of the boxplot, which shows the range where the true median should be located with 95% confidence.

![Distribution of execution time of AddressSanitizer compared to not instrumented clang.\label{benchmark_asan}](assets/asan-O2.pdf)

<!-- TODO: valgrind, axis labels? -->

In case of the Boehm GC, the results are especially interesting. The overhead stated in the paper was 13.4% using the SPEC CPU benchmark suite, while cicro measured zero overhead compared to the compiled binary. Boehm GC is a garbage collector, which is designed to automatically free unused memory. This means it expects programs to not free memory itself, otherwise this makes the necessary of a garbage collector futile. The benchmarks were never modified to address this requirement. Another problem in cicro arises due to the fast execution time of a single benchmark, which is often below 10s. The probability of a GC run is this time is quite low, and thus it can be assumed that the garbage collector did not even run while executing the benchmarks.

<!--Validity: same runtime, different benchmarking suites -->

 <!-- TODO: valgrind - disable leaking check at end of program run -->

<!-- results: vmg-crustache timeouts with diehard
bohemgc sometimes did not output anything, or printed warnings, or asserts
-->

\newpage
## Repeatability

<!-- TODO: cyte repeatability (https://www.acm.org/publications/policies/artifact-review-badging) -->

Repeatability means that consecutive measurements yield the same results. We can evaluate this property on the aggregated benchmark results as well as for each individual benchmark. Benchmarks which are unstable due to their internal operating principles, are not sensible to be included in the benchmark-suite. One example for such benchmarks are garbage-collectors which cannot be controlled in a meaningful way.

Beside of the benchmark itself, the hardware, the operating system, as well as user-space programs can interfere with actual measurements. It is practically impossible to do consecutive program-runs where the number of CPU-cycle stays the same.[@weaver2013non; @weaver2008can] For practical reasons, the impact of the system on the benchmark results thus has to be minimized.

A common source of interference which can easily be removed is the fact that the CPU frequency is typical not fixed, but adjusted to the workload. Deactivating CPU-Scaling and thus setting the frequency on a fixed value improves repeatability. For example, on a CPU which scales between 800MHz and 2,4GHz, the runtime can theoretically differ up to a factor of 4 only due to CPU-scaling. In case of cicro, the benchmark suite contains the script ```./tools/ondemand.sh``` which sets the CPU Performance mode and therefore deactivates CPU-scaling.

To assess the repeatability, a simple test-setup was chosen. By doing a compiler benchmark multiple times consecutively, they can be compared to each other. It was assumed, a benchmark suite with good repeatability would create the same overall performance figures each time. The results can be seen **Figure \ref{benchmark_clang}**, showing that the calculated mean differs less than $\pm0.1\%$ on consecutive runs. Those results lead to the assumption that performance differences below the 1% range can be identified using cicro.

![Comparison of consecutive runs of ''clang-O3'' correlated to a reference ''clang-O3'' run.\label{benchmark_clang}](assets/clang-O3.pdf)

<!-- TODO: CPU-pinning -->
<!-- TODO: cache misses -->
<!-- Tabelle mit benchmark repeatability -->
<!-- inklusive varianz innerhal eines durchlaufes -->

## Docker Overhead

When benchmark execution is not done directly on the host system, but abstracted by an operating-system-level virtualization, it needs to be verified that the performance numbers are equivalent. It has been evaluated that docker should not impact CPU and memory usage [@preeth2015evaluation; @felter2014ferreira], and therefore overhead should be not be visible when compared to the execution on the host system. While I/O and OS interaction can create noticeable overhead in docker, most benchmarks should focus on raw computation and therefore not be affected.

To evaluate the overhead when running benchmarks inside docker, the same benchmarks are run one docker as well as on the host system. When there is no overhead observable, docker execution should lead to the same performance results. In case of an observable overhead, benchmarking in docker container can still be done, as long as the resulting numbers are the same compared to the ones obtained by benchmarking directly on the host system. Thus, only comparing docker executor results with docker executor results, and host executor results with host executor results.

For evaluation, four benchmark runs were done. Using clang, two optimization levels were tested (O0 and O3), and executed on the host system as well as the docker executor. For evaluation, the host execution was used as reference and a plot was generated for each optimization level. The results can be seen in **Figure \ref{benchmark_docker_O0}** and **Figure \ref{benchmark_docker_O3}**. The measured overhead was below $\pm0.1\%$ and thus below the detection limit of this test setup. Especially the run with O0 showed no noteworthy difference of the measured mean. This can be explained with the higher execution time of the benchmarks due to the low optimization level. On contrast, the results shown in **Figure \ref{benchmark_clang}** had been determined using the highest optimization level of clang, and thus required less time to exe.

![Comparison of host and docker execution using ''clang-O0''. Host is used as reference.\label{benchmark_docker_O0}](assets/clang-docker-O0.pdf)

![Comparison of host and docker execution using ''clang-O3''. Host is used as reference.\label{benchmark_docker_O3}](assets/clang-docker-O3.pdf)

\newpage
## Type of Benchmarks

Because the benchmarks were derived from real-world projects using a specific set of rules, there was no mean of control which type of projects would be added to the benchmark suite. It is therefore of interest to have at least a rough summary of the projects used for a benchmark implementation.

This classification was done by a single person, separating the projects into classes which looked appropriate. Thus, the classification given in **Table \ref{case_studies_types}** can be seen as subjective done on a best-effort basis. Nonetheless, it is sufficient to get a basic overview of the benchmark suite.

: Overview of benchmarks implemented, separated by type.\label{case_studies_types}

+------------------+----------+-----------------------------------------------------+
| Type             | Projects | Description                                         |
+==================+==========+=====================================================+
| data-structure   | 13       | data structures like heap, trees,...                |
+------------------+----------+-----------------------------------------------------+
| math             | 7        | math library to implement e.g. matrix calculations  |
+------------------+----------+-----------------------------------------------------+
| interpreter      | 6        | interpreter for various scripting languages         |
+------------------+----------+-----------------------------------------------------+
| strings          | 6        | string handling including string analysis           |
+------------------+----------+-----------------------------------------------------+
| crypto           | 5        | implementation of cryptographic algorithms          |
+------------------+----------+-----------------------------------------------------+
| serialization    | 5        | data serialization and deserialization              |
+------------------+----------+-----------------------------------------------------+
| special          | 5        | projects which did not fit into the other types     |
+------------------+----------+-----------------------------------------------------+
| compression      | 4        | compression and decompression algorithms            |
+------------------+----------+-----------------------------------------------------+
| parser           | 4        | parser for data structures, like JSON               |
+------------------+----------+-----------------------------------------------------+
| machine learning | 3        | neural network implementations                      |
+------------------+----------+-----------------------------------------------------+
| multi-threading  | 2        | libraries to simplify multithreading integration    |
+------------------+----------+-----------------------------------------------------+
| sorting          | 1        | algorithms like quicksort and selection sort        |
+------------------+----------+-----------------------------------------------------+

It can be concluded that the number of projects in each class differs. On the other hand, the benchmark suite contains a mix of different type of projects. Ranging from simple data-structures and algorithms, up to whole neural networks and programming languages. In practice, this resulted in a benchmarking suite which is able to do successful benchmark runs while exposing limitations and bugs in runtime environments. Due to the high number of benchmarks, one or two failing ones do not cause the overall benchmarking process to fail, and results can still be evaluated in a sensible way.

<!-- TODO: explain which projects were added into special --->


## Bugs Found and Fixed

During the development of this benchmark harness, various bugs were found. Both, in the code repositories on which the benchmarks are based on, as well as in libraries used by our benchmark harness. Moreover, multiple bugs were found in the benchmarked runtime environments.

In sum, 9 issues were reported on GitHub on projects used as basis for benchmarks. In **Table \ref{bugs_benchmark}**, a overview is given including the number of issues fixed. In 7 cases patches were submitted alongside the issue report, where the majority of them is now incorporated into the upstream project. Most of the issues were found during the process of evaluating the performance of bug-finding tools.

<!--
https://github.com/antirez/otree/pull/3
https://github.com/antirez/otree/pull/4
https://github.com/antirez/otree/pull/5

https://github.com/chrismoos/hash-ring/issues/19

https://github.com/snowballstem/snowball/pull/84

https://github.com/bashrc/libdeep/pull/12  repository no longer available

https://github.com/zserge/partcl/issues/6

https://github.com/watmough/jwHash/pull/6

https://github.com/mbrossard/threadpool/pull/13
-->

: Overview of bugs reported and fixed per project.\label{bugs_benchmark}

+-----------------------+---------------+------------+--------------------------------------------+
| Benchmark             | Reported      |  Fixed     | Comment                                    |
+=======================+===============+============+============================================+
| antirez/otree         | 3             | 2          | valgrind errors, c-specification violation |
+-----------------------+---------------+------------+--------------------------------------------+
| chrismoos/hash-ring   | 1             | 1          | memory leak                                |
+-----------------------+---------------+------------+--------------------------------------------+
| snowballstem/snowball | 1             | 1          | compiler flags are not passed correctly    |
+-----------------------+---------------+------------+--------------------------------------------+
| bashrc/libdeep        | 1             | 1          | uninitialized memory, memory leak          |
+-----------------------+---------------+------------+--------------------------------------------+
| zserge/partcl         | 1             | 0          | segmentation fault                         |
+-----------------------+---------------+------------+--------------------------------------------+
| watmough/jwHash       | 1             | 1          | build broken                               |
+-----------------------+---------------+------------+--------------------------------------------+
| mbrossard/threadpool  | 1             | 0          | POSIX-specification violation              |
+-----------------------+---------------+------------+--------------------------------------------+

When looking on issues emerged during the integration of new runtime environments, build issues are the most prominent ones. There is a high number of runtime environments which could not be added, due to missing build information, outdated requirements, or which are fail to build anyway even with the correct dependencies. In case of Sulong, a serious performance bottleneck could be triggered using cicro, which is now fixed.[@sulong:issue1]

<!--
https://github.com/graalvm/sulong/issues/824
https://github.com/oracle/graal/issues/794


https://github.com/vusec/dangsan/issues/1

https://github.com/jtcriswell/safecode-llvm37/issues/4
-->

With the addition of hardware counter support, PAPI [@terpstra2010collecting] was integrated into C-Hayai. During integration, a build issue was encountered due to the addition of a new error message in the recent GCC version. One of the dependent libraries triggered this error and caused the built to fail by default. This issue serves as good example why it is a good idea to automatically test projects on recent compiler and library versions to spot issues quickly. Minimizing the number of warnings in a project is also a strategy to mitigate possible future build errors.

<!--
https://bitbucket.org/icl/papi/issues/46/cannot-compile-on-arch-linux
https://sourceforge.net/p/perfmon2/bugs/7/

https://bitbucket.org/icl/papi/issues/49/minimum-example-already-has-memory-leaks
-->
