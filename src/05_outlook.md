
\newpage
# Outlook

<!-- Questions:

* what could be done as well?
* what was planned but never realized?

--->

This benchmark suite tries to improve performance evaluation in research and engineering. With a focus on helping in the compiler as well as runtime development, it has to give expressive results while being easy enough to use for wide adoption.

<!-- Additional Benchmarks -->
One of the main arguments for this benchmark-suite is the number of benchmarks implemented. A bigger number of benchmarks allows portraying a wider variety of program execution paths as well as programming styles. Increasing the number of benchmarks therefore would make sense to further expand the variety to benchmarks.

<!-- micro benchmarks -->
Currently, the suite consists of benchmarks based on real-world code found on GitHub. This approach has the advantages to get a wide variety of code. On the other hand, this does not guarantee us to have a good estimation of the real world. After all, most benchmarks in cicro only test parts of the original application, and most of the included application are below 10000 lines of code. One addition could be therefore the addition of bigger application, which would on the other hand increase complexity of the benchmark suite. A different approach, which could be employed to catch performance bottlenecks, would be the addition of micro-benchmarks written specifically to trigger them. This would give the theoretical advantage to better map the best-/worst-case performance of the evaluated runtime environment. On the practical side, it is not really possible to know beforehand what performance bottlenecks specific runtime environments have.

<!-- differ in suites -->
While the overview of the benchmark-suite lists the type of projects used, it does not really split them into more specialized suites to evaluate. In case of the Spec CPU Benchmark, two suites exist, one for integer, the other one for floating-point. Separating cicro into multiple parts with various specializations could help to improve interpretation of performance metrics. Especially, when those metrics are heavily affected by such a specialized suite.

<!-- multithreading -->
Most of the benchmarks in cicro are single-threaded. This means, it would be possible to run multiple of the benchmarks in parallel to speed up a full benchmark-run. Additionally. On modern systems, it needs to be noted that hyperthreading can influence the performance in a significant way, and therefore has to be avoided when implementing parallel benchmark runs.

<!-- CPU bind -->
On multiprocessor systems, the scheduler has the ability to move processes from one CPU core to another. It is possible to bind a process to a specific CPU core, and even better, keep the core free from any other applications. Doing so would allow minimizing the influence of the system on the benchmark quite a bit.

<!-- detect more system abnormalities -->
Cicro is already detecting system settings like the used CPU scaling governor, to check if the system is correctly configured. To do accurate measurements, influences of the system have to be minimized as much as possible. By extending the detection scripts, accuracy can be improved further on. For example by detecting high CPU-load load which would cause imprecise benchmark results.

<!-- more evaluation -->
