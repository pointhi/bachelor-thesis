
\newpage

# Glossary

benchmark harness

:   The main framework which is used by every benchmark file to handle the measurements and the output of results afterwards.

benchmark file

:   A single file which executes one or more benchmarks. It can be run as standalone application, but is typically invoked by the main benchmark harness.

benchmark suites

:   A project which allows users to benchmark specific metrics. It is based on a benchmark harness which is used by the specific benchmark.

runtime environment

:   A tool which is benchmarked. It is based on a specific application (e.g. bug finding tool), as well as the compiler flags and environment variables used. A runtime furthermore is extended by a function which reads the version of the tool used.
