all: out.pdf

# Pandoc stuff
out.pdf: template/* src/* assets/*
	pandoc -F pandoc-citeproc src/*.md -o out.pdf --from markdown --toc --bibliography=src/references.bib --csl=template/acm-sig-proceedings.csl --template template/eisvogel.tex --listings --number-sections

watch: out.pdf
	while true; do inotifywait -e close_write template/* src/* assets/*; $(MAKE); done

# Entity-Relationship Diagram Generator
build_erd: assets/db_schema.er
	./tools/erd/generate.sh assets/db_schema.er assets/db_schema.pdf

watch_erd: build_erd
	while true; do inotifywait -e close_write assets/db_schema.er; $(MAKE) build_erd; done
