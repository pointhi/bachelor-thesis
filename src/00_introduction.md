---
title: "Cicro: A C Benchmarking Suite"
author: [Thomas Pointhuber]
institute: "Institute for System Software"
supervisor: "Manuel Rigger"
date: "May 2019"
keywords: [Benchmarking, C]
titlepage: true
abstract: \null\vspace{\fill}
 \section*{Abstract}
 Quantifying improvements in the area of compiler and runtime development is often done using either proprietary or custom designed benchmarking suites. One representative is the SPEC CPU benchmark suite, which is frequently used to measure the performance differences of compiler improvements. This bachelor thesis proposes an alternative benchmark suite called cicro, which is free to use and suitable for compiler and runtime development. Notable characteristics which set it apart from other benchmarking suites is the usage of existing open source projects as benchmark sources, the integration of hardware counter support in the benchmark harness and the usage of Docker to unify the benchmarking process. Results showed that cicro can measure performance differences below 1\% reliable, and does not expose measurable execution time differences when running inside Docker virtualization, instead of running on the host system directly.
 \vspace{\fill}\vspace{\fill}\vspace{\fill}\vspace{\fill}
 \newpage
...

\newpage
# Introduction

<!-- Questions:

* what is cicro?
* why using cicro?
* what are the usp?
--->

Benchmarking is necessary to evaluate the performance of applications. In case the performance of an application should be improved, the actual impact of the software modifications has to be measured. Without measuring the performance impact, it cannot be guaranteed that the modified application is actually faster than the old version. In case of compiler construction, as well as related areas like runtime development, doing such benchmarking can be quite complicated. First of all, it is important to note that performance is often affected by different parts of an application. For example, loop unrolling should improve performance of tight loops due to less instructions executed, but has the downside of increased code size. When a function calls the unrolled section repeatedly, the increased code size can lead to cache misses. Thus, in practice heuristics are used to control the part where optimizations should be applied. To actually improve performance, a trustworthy benchmarking suite is required to evaluate the actual impact of an optimization pass on applications.

At the moment, SPEC CPU Benchmark [@henning2000spec] is such a benchmark suite often used for evaluation. In practice, results of SPEC CPU Benchmark are visualized to some baseline where each benchmark is shown separately. Now, the question arises if this is actually a sensible way to do performance evaluation. It is easy to identify the measured minimum and maximum overhead, but the low amount of benchmarks can miss special cases rather easy. Another point of concern is that the low amount of benchmarks potentially allows tailoring the evaluated software towards the benchmarks, thus, reducing the generalizability of the results. Execution time can be considered relevant as well, because the SPEC CPU Benchmark suite requires quite some time to complete. Some of the benchmarks require over 10 minutes for one execution. Combined with the costs of around $1000 to obtain a license, using SPEC CPU Benchmark can be considered cumbersome to simply get some performance measurements in a timely manner.

<!--This thesis proposes cicro as a cost-free solution. By utilizing a high number of projects found on GitHub, it is an extensive and practical oriented benchmarking suite. Short test durations allow the user to balance between execution time and accuracy. With the concept of using Docker as benchmark runner, cicro allows the simple creation of reproducible benchmarking runs. Reproducing results was always a point of concern in research, and docker allows this in a timely manner. By integrating docker support into the basic workflow, reproducibility of benchmarks should improve.-->

<!-- TODO:
* improve thesis proposabl to be concret
* why docker?
* motivation
* main build_docker_image
* how evaluated
* main findings/insights
* describe benchmark selection process
-->

This thesis proposes the benchmark suite *cicro* as a cost-free alternative. One of the ideas cicro is build on is to minimize the personal influence on the selection process using existing open source projects. Initially, we specified criteria to select a set of suitable projects, like the platform the project can be found on, number of stars and the license of the project itself. Build upon this set of projects, each was evaluated for its suitability as benchmark, and if considered suitable, a benchmark was implemented. Another idea for cicro was to speed up benchmark execution, while still being accurate. Measurements of repeatability showed that cicro is accurate enough to measure performance differences below 1%. This accuracy is achieved in cicro with an execution time of at least 12 minutes for a full benchmark run including compilation.

To improve reproducibility, an operating-system-level virtualization called Docker was chosen to be included in the supported workflows of cicro. The intention to introduce virtualization to a benchmark suite had two reasons: Simplifying the execution of different versions of runtime environments on the same system, and to unify the way of building and executing benchmarks.
