
\newpage
# Usage

<!-- Questions:

* how to install?
* how to run simple example?
* how to customize for my stuff?
* what is possible using it?

--->

While the benchmark suite tries to be as easy as possible to use, some documentation is still necessary. There are a few requirements to get the benchmark suite up-and-running, and users need to know how to actually interact with the benchmark suite. This includes running a basic benchmark run, plotting results as well as extending the benchmark suite to support custom runtime environments.

## Install Dependencies

The benchmark suite can be seen partitioned into the parts written in C, the parts written in Python and the parts written in R. For the C part, a compatible compiler as well as the basic Linux build tools and ```cmake``` are required. Optionally, when hardware counter support is required, the library ```libpapi``` has to be installed as well. In **Listing \ref{lst:install_dependencies}** an example installation of all dependencies is shown.

For Python, the dependencies are defined in ```requirements.txt```. Namely ```psutil``` to extract system information, ```python-dateutil``` to handle time data and ```sqlalchemy``` as SQL toolkit and object-relational mapper are required. Those Python dependencies can either be installed using the package manager of the distribution, or by using the Python package manager.

The R scripts require additional packages as well. For simplification, the script ```requirements.R``` can install all of them. Namely ```tidyverse``` which is a collection of data science packages, ```optparse``` for argument parsing and ```jsonlite``` to support parsing of JSON files. All of those named dependencies can be found in the cran repository. Furthermore, ```klmr/modules``` has to be installed from GitHub, which is a module manager to import self written R libraries. To actually install those packages on the system, all required dependencies need to be installed beforehand to have the R package manager successfully build them.



```{#lst:install_dependencies .bash caption="Install dependencies on Ubuntu 18.04"}
# install basic benchmark dependencies + Python packages and docker
apt-get install build-essential cmake libpapi-dev python3 \
    python3-psutil python3-dateutil python3-sqlalchemy docker.io

# alternative way to install Python packages:
# pip3 install -r requirements.txt

# install R Packages
apt-get install r-base gcc g++ libcurl4-gnutls-dev littler \
    libssl-dev libxml2-dev
./requirements.R
```

<!-- TODO: ## Program arguments

required arguments, optional arguments -->

## Run Benchmarks Inside Docker

The docker executors take over parts of the setup process, and handles the execution inside a operating-system-level virtualized environment. All scripts required to built and run on the docker system are located in the directory ```./docker/```. Using the docker executor is especially important when trying to benchmark multiple runtime environments which are incompatible to each other. For example because they depend on different libc versions. By abstracting the user-space components it is possible to run them on the same system without a hassle.

<!--It should be obvious docker has to be installed on the system. Furthermore, to simplify creating and running the docker images, Makefiles are used. **Listing \ref{lst:install_docker}** shows how to install those dependencies on a Ubuntu system.

```{#lst:install_docker .bash caption="Install dependencies for docker executor on Ubuntu 18.04"}
apt-get install make docker.io
```
-->

To get the docker images running, they need to be available on the system first. There are two ways to achieve this: built them from scratch, or use existing ones. In **Listing \ref{lst:build_docker_image}** both variants are described. Using existing images has the advantage to be faster (depending on the internet connection), and to improve replicability by using the exact same image other users use. Building from scratch is especially important when new docker executors are added which do not exist yet, or existing ones are altered. Adding a new runtime environment also requires a rebuild to address the change in the docker containers which are supporting.

```{#lst:build_docker_image .bash caption="Get ``cicro/benchmark-clang-7`` docker image available on the system."}
# build docker image from scratch (including base images)
make -C ./docker build_BENCHMARK_CLANG-7

# alternative: pull existing docker image from docker hub
# make -C ./docker pull_BENCHMARK_CLANG-7
```

To actually run the benchmarks, the Makefile of the specific docker executer has to be used. It is possible to modify some execution variables to change for example the output directory or set arguments of the ```./tools/bench.py``` script. A simple example which shows how to do this is shown in **Listing \ref{lst:run_docker_image}**. It uses the docker image added to the system before, and overwrites the runtime filter. Furthermore, the output directory is pointed to a custom directory instead of the default directory located next to the Makefile.

<!-- TODO: backtick not visible in document -->
<!-- TODO: squeeze layout a bit to keep on same page -->
```{#lst:run_docker_image .bash caption="Run ``cicro/benchmark-clang-7`` docker executor with ``clang-O0`` and ``clang-O3``."}
make -C ./docker/BENCHMARK_CLANG-7 run OUTPUT_DIR=`pwd`/out \
    BENCHMARK_ARGS='--filter-runtime="(clang-O0|clang-O3)"'
```

## Run Benchmarks on Host-system

All dependencies required to run the desired runtime need to be installed on the host system. In case the benchmark uses an environment variable to find the runtime environment, the path to the target can be defined in ```./configs/env```. The benchmark harness tries to automatically store the version of the runtime environment used for documentation purpose.

To execute the benchmarks, the script ```./tools/bench.py``` is used. It requires at least a target file to be defined, where the results are stored. Generally, it is desired to only execute the runtime environments the user requires. To do this a runtime filter can be specified, which is a regular expression checking if it matches with the runtime name. It is possible to output all registered runtime environments as shown in **Listing \ref{lst:bench_list_runtimes}** to check which ones are available. This command also allows users to check if there are environment variables required to be defined, like ```${GCC}``` in this example.

```{#lst:bench_list_runtimes .bash caption="List all runtime environments from gcc-O0 to gcc-O3."}
# for compactness only show runtimes matching "^\s*gcc-O[0-3]"
./tools/bench.py --list-runtimes | grep "^\s*gcc-O[0-3]"
 gcc-O0     | {'CC': '${GCC}', 'AS': 'as', 'CFLAGS': '-std=gnu99 -O0'}
 gcc-O1     | {'CC': '${GCC}', 'AS': 'as', 'CFLAGS': '-std=gnu99 -O1'}
 gcc-O2     | {'CC': '${GCC}', 'AS': 'as', 'CFLAGS': '-std=gnu99 -O2'}
 gcc-O3     | {'CC': '${GCC}', 'AS': 'as', 'CFLAGS': '-std=gnu99 -O3'}
```

In case we want to benchmark ```gcc-O0``` and ```gcc-O3```, we can tell ```./tools/bench.py``` to only use those two runtime environments and ignore all other available. Because the runtime environment requires the environment variable ```${GCC}``` to be defined, the tool path has to be declared. An example entry to declare environment variables is shown in **Listing \ref{lst:env_gcc}**. The actual invocation for this benchmark run is shown in **Listing \ref{lst:bench_gcc}**. As explained before, filtering for a runtime environment is done using a regex.

```{#lst:env_gcc .py caption="Example entry in ``./configs/env`` to declare location of GCC executable."}
GCC=/usr/bin/gcc
```

```{#lst:bench_gcc .bash caption="Benchmark ``gcc-O0`` and ``gcc-O3`` runtime environment and write results to output.json"}
./tools/bench.py output.json --filter-runtime="(gcc-O0|gcc-O3)"
```

## Plot Results

The evaluation of benchmark results has to be done in different ways, depending on the purpose. For example, when comparing hardware performance in general, absolute numbers makes sense. In case a general score has to be computed, this type of performance number requires careful assessment to weight different metrics appropriate. When doing software optimization, a different information is of relevance: how much did the performance changed compared to the previous version. Now, the important part is not the general performance across systems, but the actual impact of a change. Performance numbers therefore need to represent the impact of a change on the system, like if the system is faster in general, and if the change introduced cases where performance drops in a significant way.

In case of cicro, benchmark results are always compared to some base, which is run on the same system as reference. This allows us to measure performance differences between the reference and the measured runtime environment. To do so, at least two benchmark runs with different runtime environments are required.

To use the plot function in cicro, the user has to supply the script with the benchmark data, as well as a specification which runtime environments is considered as the base. An example command to plot benchmark results is shown in **Listing \ref{lst:plot_results}**. In this case, ```clang-O0``` is considered the base. The resulting plots are always written to the file ```Rplots.pdf``` in the current working directory.

```{#lst:plot_results .bash caption="Plot comparison of ``clang-O0-v7`` and ``clang-O3-v7`` with ``clang-O0-v7`` as base runtime."}
./tools/plot.R --benchfile=`pwd`/out/clang-7.0.json \
               --base-runtime=clang-O0-v7
```

The base runtime results are all normalized to represent 1, and every other runtime environment is normalized to the base results. In the plot, a benchmark is therefore always seen in comparison to the base runtime. For example a benchmark run is two time slower than the base benchmark run. Comparing results in absolute numbers would not make much sense, because the execution times of different benchmarks can be very different. By doing a normalization, all benchmark results can be combined into one sensible metric which is then used for evaluation.

\newpage
## Adding a New Runtime Environment

While there are already over 100 runtime environment configurations present by default in cicro, missing environment configurations need they own custom configurations. In case a missing runtime environment should be benchmarked, a new runtime environment configuration has to be added. This allows cicro to integrate new configurations seamlessly, and benchmark them alongside existing ones.

An example configuration was already shown in **Listing \ref{lst:scripts_gcc}**, which added two configurations for gcc alongside a helper function to retrieve version information. A more complex configuration of a runtime environment is shown in **Listing \ref{lst:complex_runtime_environment}**. In case of ```lli```, which is an executer for LLVM bitcode [@LLVM:CGO04], code compiled to a machine language cannot be used. Thus a helper function is required which generates the intermediate language called LLVM bitcode and executes the runtime environment correctly. Cicro has predefined helpers to simplify the implementation of runtime environments based on LLVM bitcode. Only minimal adaptions are necessary to get this configuration running. Basically, the make and exec function need to replaced with the helper functions, and a suitable compiler has to be set which allows to output LLVM bitcode.

```{#lst:complex_runtime_environment .python caption="Minimum runtime environment example for ``lli`` using predefined helper functions."}
from functools import partial

def lli_executor(filepath, workdir, args, **kwargs):
    return wllvm_executor(filepath, workdir, '${LLI}', args, **kwargs)

lli_env = {
    "CC": "${WLLVM_DIR}/wllvm",
    "AS": "${WLLVM_DIR}/wllvm",
    "CFLAGS": "-Wno-everything -O1",
    "PAPI": 0  # disable support for hardware counters
}

lli_kwargs = {
    'build_system_func': partial(build_system_executor,
         cc_version='--version', as_version='--version'),
    'make_func': wllvm_make,
    'exec_func': lli_executor
}

harness.add_runtime('lli-O1', lli_env, **lli_kwargs)
```

\newpage
## Add a New Benchmark

<!-- TODO: reference https://github.com/jku-ssw/cicro-benchmarks/blob/63c7d3fdcd85baaf30aa5f413b5423cc551a25f9/add_new_test.py -->

For normal usage, users should not be required to add additional benchmarks. After all, new benchmarks should only be introduced with a new official versions of the benchmark suite to be comprehensible. Nonetheless, it makes sense to document how the benchmark harness is used and how it interacts with the benchmark itself. All benchmarks are located in the ```benchmarks``` directory, and the addition of a benchmark requires the modification of multiple files. We can differentiate between three types of resources in the ```benchmarks``` directory:

* The file ```./benchmarks/Makefile``` controls how to built and clean benchmark artifacts.
* The file ```./benchmarks/<benchmark_name>_test.c``` is represents the benchmark itself.
* The directory ```./benchmarks/<benchmark_name>/```  contains all additional build files.

The actual benchmark file executed ends with the suffix ```_test.c``` and is located in the ```./benchmarks``` directory. An example benchmark file is shown in **Listing \ref{lst:benchmark_example}**, which uses a syntax based upon the original C-Hayai syntax. Some improvements were made to support new features like argument handling. A benchmark file supports an arbitrary number of benchmarks, which are called consecutively on a run.

```{#lst:benchmark_example .c caption="Example how a benchmark file looks like containing two benchmarks."}
#include "chayai.h"  // needs to be the first include!

// <name>, <fixture>, <default-runs>, <iterations>
BENCHMARK(example, bench1, 10, 1) {
    // benchmark code is located here
}

BENCHMARK(example, bench2, 10, 1) {
    // benchmark code is located here
}

int main(int argc, char** argv) {
    REGISTER_BENCHMARK(example, bench1);
    REGISTER_BENCHMARK(example, bench2);

    RUN_BENCHMARKS(argc, argv);
    return 0;
}
```

\newpage
Besides writing the benchmark file itself, cicro needs to know how to built the new benchmark and their dependencies. To do so, ```./benchmarks/Makefile``` has to be edited, to address the addition. Such an entry is shown in **Listing \ref{lst:benchmark_makefile}**, which reveals that there are multiple variables present to control the build step. To have some overview, the variables are described in more details now in **Table \ref{make_variables}**.

: Explanation of important variables used in the Makefile.\label{make_variables}

+--------------------------+------------------------------------------------------------------+
| Variable                 | Description                                                      |
+==========================+==================================================================+
| ```$(CC)```              | Compiler which is used to compile the benchmark.                 |
+--------------------------+------------------------------------------------------------------+
| ```$(CFLAGS)```          | Custom compiler flags which can be added by benchmark harness.   |
+--------------------------+------------------------------------------------------------------+
| ```$(LDFLAGS)```         | Custom linker flags which can be added by benchmark harness.     |
+--------------------------+------------------------------------------------------------------+
| ```$(TEST_DIR)```        | Directory where the executable should be written to.             |
+--------------------------+------------------------------------------------------------------+
| ```$(TEST_SUFFIX)```     | Contains "test" by default, can be used to create multiple binaries. |
+--------------------------+------------------------------------------------------------------+
| ```$(C_HAYAI_LIB)```     | Path to compiled benchmark harness.                              |
+--------------------------+------------------------------------------------------------------+
| ```$(C_HAYAI_CFLAGS)```  | Compiler flags required for benchmark harness.                   |
+--------------------------+------------------------------------------------------------------+
| ```$(C_HAYAI_LDFLAGS)``` | Linker flags required for benchmark harness.                     |
+--------------------------+------------------------------------------------------------------+

```{#lst:benchmark_makefile .Makefile caption="Simple specification of the build step of a benchmark file in the Makefile."}
########## <url-to-benchmark-source>

example_$(TEST_SUFFIX): example_test.c $(C_HAYAI_LIB)
    $(CC) $(CFLAGS) example_test.c $(C_HAYAI_CFLAGS) \
     -o "$(TEST_DIR)/example_$(TEST_SUFFIX)" \
       $(LDFLAGS) $(C_HAYAI_LDFLAGS)
```

Adding a new target to the Makefile is only a part of the modifications required. To actually build the target when executing the command ```make all```, the target name has to be added to the ```PROJECTS``` variable defined at the beginning of the Makefile. After finishing those steps, the new benchmark file should automatically be detected by cicro and used in the next benchmark execution.
